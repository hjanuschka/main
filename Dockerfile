FROM ruby:2.5
RUN mkdir /app

ADD Gemfile /app
ADD migrate.rb /app
WORKDIR /app
RUN bundle install

CMD echo  "please run it like\n docker run -e GITLAB_URL=https://yourgitlab.com/api/v4 GITLAB_TOKEN=yourtoken GROUP_ID GROUP_ID \n"
