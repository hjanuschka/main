Main ✊✊🏻✊🏼✊🏽✊🏾✊🏿



## About
> get rid of `master` branch!


| CLI | Activity |
|----------|-------------|
| ![CLI](assets/run.png) | ![activity](assets/activity.png) |




what does it do:
  - renames master branch into main
    - creates a backup tag `backup-before-main`
    - creates `main`  branch
    - changes default branch to `main` (if master was used)
    - iterates over open-MR's that have `master` as target branch, and changes this.



# How run:

## terminal

```bash
export GITLAB_URL=https://gitlab.krone.at/api/v4 #your gitlab host
export GITLAB_TOKEN=yourprivatetoken # create in your profile /profile/personal_access_tokens

bundle install
bundle exec ruby migrate 80 82 83
```

whereas `80 82 83`  revers to one or more group id's. as shown in the "details" tab on your groups page inside gitlab.



## using docker

```bash
export GITLAB_URL=https://gitlab.krone.at/api/v4 #your gitlab host
export GITLAB_TOKEN=yourprivatetoken # create in your profile /profile/personal_access_tokens

docker build -t gitlab_main_migrate .
docker run  -e GITLAB_TOKEN=$GITLAB_TOKEN -e GITLAB_URL=$GITLAB_URL g bundle exec ruby migrate.rb 80 82 83
```



