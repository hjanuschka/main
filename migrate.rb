# frozen_string_literal: true

require 'gitlab'
require 'pry'
module KRN
  class NoMoreMaster
    BACKUP_TAG = 'backup-before-main'
    PER_PAGE = 100
    def initialize
      Gitlab.configure do |config|
        config.endpoint       = ENV['GITLAB_URL']
        config.private_token  = ENV['GITLAB_TOKEN']
      end
      @client = Gitlab.client
    end

    def has_branch(project: nil, branch: 'master')
      @client.branch(project.id, branch)
      true
    rescue StandardError => e
      false
    end

    def handle_groups(group_ids: [])
      # get all projects
      projects = []
      group_ids.each do |group|
        projects += @client.get("/groups/#{group}/projects").auto_paginate.select do |proj|
          has_master = has_branch(project: proj, branch: 'master')
          has_main = has_branch(project: proj, branch: 'main')
          if has_master && !has_main
            proj
          else
            false
          end
        end
      end

      puts "Found: #{projects.length} projects"

      projects.each do |project|
        # create a tag - for backup purposes
        @client.create_tag(project.id, BACKUP_TAG, 'master')
        puts "Working on: #{project.path_with_namespace}"
        puts "\t Created a backup tag '#{BACKUP_TAG}' 🏷️"
        # create main branch
        @client.create_branch(project.id, 'main', 'master')
        puts "\t Created 'main' branch ✊✊🏻✊🏼✊🏽✊🏾✊🏿"

        if project.default_branch == 'master'
          @client.put("/projects/#{project.id}?default_branch=main")
          puts "\t changed default branch to 'main' 🧹"
        end

        # iterate over MRS
        # find those with 'master' as a target
        # change target to main

        @client.merge_requests(project.id, per_page: PER_PAGE).each do |tmr|
          if tmr.target_branch == 'master' && tmr.state == 'opened'
            puts "\t Migrating MR #{tmr.iid} #{tmr.title}"
            @client.update_merge_request(project.id, tmr.iid,	target_branch: 'main')
          end
        end

        # delete master
        @client.delete_branch(project.id, 'master')
        puts "\t Deleted 'master' branch ✅"
      end
    end
  end
end

group_ids = ARGV

main = KRN::NoMoreMaster.new
main.handle_groups(group_ids: group_ids)
